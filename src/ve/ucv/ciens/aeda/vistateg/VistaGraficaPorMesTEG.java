/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ve.ucv.ciens.aeda.vistateg;

import java.awt.image.BufferedImage;
import java.io.*;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;

/**
 *
 * @author Audel
 */
public class VistaGraficaPorMesTEG extends javax.swing.JFrame {

    /**
     * Creates new form VistaraficaPorMesTEG
     */
    public VistaGraficaPorMesTEG(int [] req, String selYearMes) {
        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        
        initComponents();
        
        //Variables usadas para el archivo de salida y la ejecucion del generador de graficas
        String codEjecucion = "java -jar GeneradorGraficasTEG.jar salidatemporal.txt";
        String pathArchivoSalida = "C:\\Users\\Audel\\Desktop\\salidatemporal.txt";
        
        //VARIABLES PARA LA BUSQUEDA EN BD POR MES
        String fechaInicio = selYearMes+"-01";
        String fechaFin = selYearMes+"-31";
        
        //VARIABLES PARA USAR EN EL ARCHIVO
        Boolean soloUltJuego = false;
        if(req[3] == 1){
            soloUltJuego = true;
        }
        Boolean ultiFechaNotSet = true;
        Date ultimaFecha = new Date(2006, 12, 31);
        String lineaEjeX = "";
        String lineaEjeY = "";
        String nombrePeriodo = "";
        String tokenBusquedaEst = "";
        if(req[3]==1){
            nombrePeriodo = "(En el mes " + selYearMes +")";
            lineaEjeX = "Valores del Juego Durante Mes Seleccionado";
        }else if(req[3]==2){
            nombrePeriodo = "(Historico de Juegos) ";
            lineaEjeX = "Fechas Sesiones de Juego";
        }
        String nombreJuego = "";
        if(req[1]==1){
            nombreJuego = "Puzzle ";
        }else if(req[1]==2){
            nombreJuego = "Viste al Personaje ";
        }else if(req[1]==3){
            nombreJuego = "Fabrica ";
        }else if(req[1]==4){
            nombreJuego = "Pintar y Rellenar ";
        }
        String nombreDato = "";
        if(req[2]==1){
            nombreDato = "Tiempo de Juego ";
            lineaEjeY = "Tiempo de Juego (segundos)";
            tokenBusquedaEst = "tiempo";
        }else if(req[2]==2){
            nombreDato = "Separacion de Dedos ";
            lineaEjeY = "Separacion de Dedos (mm)";
            //este dato se refiere al promedio de separacion de dedos
            tokenBusquedaEst = "promedios";
        }else if(req[2]==3){
            nombreDato = "Porcentaje de Relleno ";
            lineaEjeY = "Procentaje de Area Rellenada";
            //buscara por pronedio de porcentaje relleno de fira
            tokenBusquedaEst = "promedios";
        }
        
        //EEJCUCION DE LA CONSULTA A BD BASADA EN LOS PARAMETROS REQUERIDOS
        Connection c = null;
        Statement stmt = null;
        PreparedStatement pst = null;
        ResultSet resultSet = null;
        try {
                Class.forName("org.sqlite.JDBC");
                c = DriverManager.getConnection("jdbc:sqlite:C:\\Users\\Audel\\Desktop\\data_destruible.db3");
                System.out.println("Opened database successfully");

                String consLoginSql = "SELECT * FROM gameTable WHERE idPatient=? AND idGame=? AND date>=? AND date<=? ORDER BY date DESC";
                try{
                        //LA "ÏMPRESION" EN EL ARCHIVO
                        PrintWriter writer = new PrintWriter(pathArchivoSalida, "UTF-8");
                        if(soloUltJuego){
                            writer.println("barra");
                        }else{
                            writer.println("serie");
                        }
                        //ESCRIBE EL TITULO DE LA GRAFICA
                        writer.println(nombreDato + nombreJuego + nombrePeriodo);
                        writer.println(lineaEjeX);
                        writer.println(lineaEjeY);
                            
                        pst = c.prepareStatement(consLoginSql);
                        pst.setString(1, String.valueOf(req[0]) );
                        pst.setString(2, String.valueOf(req[1]+1000) );
                        pst.setString(3, fechaInicio );
                        pst.setString(4, fechaFin );

                        resultSet=pst.executeQuery();
                        //primero se veriica que existan jueos y por ende la cosulta traigga datos
                        if(resultSet.next()){
                                                        
                            //intenta volver a hacer el query para ahora si guardar
                            resultSet=pst.executeQuery();
                            
                            System.out.println("Encontrados algunos juegos");
                            while(resultSet.next()){
                                String lineaRow = resultSet.getString("dataGame");
                                System.out.println(lineaRow);
                                //this.setVisible(false);
                                Date fechaSel = resultSet.getDate("date");
                                String fechaSelStr = resultSet.getString("date");
                                
                                String [] fechaSelStrSplit = fechaSelStr.split("-");
                                fechaSel = new Date(Integer.parseInt(fechaSelStrSplit[0]), 
                                        Integer.parseInt(fechaSelStrSplit[1]), 
                                        Integer.parseInt(fechaSelStrSplit[2].split(" ")[0]) );
                                System.out.println(fechaSel);
                                if(soloUltJuego){
                                    //guarda unicamente los datos cuya fecha sea igual a la primera
                                    //uuso el ooolean para el primer caso en qe no se cuual es la ultima fecha
                                    if(ultiFechaNotSet){
                                        //ultimaFecha = resultSet.getDate("date");
                                        ultimaFecha = fechaSel;
                                        ultiFechaNotSet = false;
                                        String [] lineaDataRow = resultSet.getString("dataGame").split("\\^");
                                            for(int ild=0; ild<lineaDataRow.length; ild++){
                                                System.out.println("un pedacito es " + lineaDataRow[ild]);
                                                String [] undato = lineaDataRow[ild].split("=");
                                                if(undato[0].equalsIgnoreCase(tokenBusquedaEst)){
                                                    //encontro el dato actual de la busqueda y lo mete en el archivo
                                                    writer.println(fechaSelStr + " " + undato[1]);
                                                }
                                            }
                                        //FIN DEL FOR
                                    }else{
                                        //compara todas las siguientes fechas contra la priemra para obtener unicamente la ultima sesion
                                        if(ultimaFecha.compareTo(fechaSel) != 0 ){
                                            //la fecha actual es igual a la ultima fehca asi que se guarda
                                            System.out.println("se encontro una fecha igual a la ultima");
                                            System.out.println(fechaSel);
                                            String [] lineaDataRow = resultSet.getString("dataGame").split("\\^");
                                            for(int ild=0; ild<lineaDataRow.length; ild++){
                                                System.out.println("un pedacito es " + lineaDataRow[ild]);
                                                String [] undato = lineaDataRow[ild].split("=");
                                                if(undato[0].equalsIgnoreCase(tokenBusquedaEst)){
                                                    //encontro el dato actual de la busqueda y lo mete en el archivo
                                                    writer.println(fechaSelStr + " " + undato[1]);
                                                }
                                            }
                                            //YAY CASO FASTIDIOSO
                                        }
                                    }
                                }else{
                                    //simplemente coloca todas las fechas
                                    System.out.println("una fecha del historico");
                                    System.out.println(fechaSel);
                                    String [] lineaDataRow = resultSet.getString("dataGame").split("\\^");
                                    for(int ild=0; ild<lineaDataRow.length; ild++){
                                        String [] undato = lineaDataRow[ild].split("=");
                                        if(undato[0].equalsIgnoreCase(tokenBusquedaEst)){
                                            //encontro el dato actual de la busqueda y lo mete en el archivo
                                            writer.println(fechaSelStr + " " + undato[1]);
                                        }
                                    }
                                }
                            }
                            
                            //writer.println("The first line");
                            //writer.println("The second line");
                            writer.close();
                        }else{
                            System.out.println("no encontro juegos");
                        }

                }catch(Exception e){
                        System.out.println(e);
                }

                c.close();
        } catch ( Exception e ) {
                System.err.println( e.getClass().getName() + ": " + e.getMessage() );
                System.exit(0);
        }
        System.out.println("Consulta exitosa");
        
        //UNA VEZ HAYA REALIZADO EL QUERY DEBE EJECUTAR EL JAR QUE GENERA LA GRAFICA
        ProcessBuilder pb = new ProcessBuilder("java", "-jar", "prueba.jar", pathArchivoSalida);
    	pb.redirectErrorStream(true);
    	pb.directory(new File("C:\\Users\\Audel\\Desktop"));

        //booleano para saber si carga la imamgen
        Boolean succesImage = false;
        
    	System.out.println("Directory: " + pb.directory().getAbsolutePath());
    	Process p;
        String pathline = new String();
        try {
            p = pb.start();
            
            InputStream is = p.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            for (String line = br.readLine(); line != null; line = br.readLine()) {
                    System.out.println( line ); // Or just ignore it
                    //verifica si hay la linea scces
                    if(line.equalsIgnoreCase("SUCCESS")){
                        succesImage = true;
                        pathline = br.readLine();
                    }
            }
        } catch (IOException ex) {
            Logger.getLogger(VistaGraficaTEG.class.getName()).log(Level.SEVERE, null, ex);
        }
    	System.out.println("finalizo la ejecucion y debe cargar la imagen");
        
        if(succesImage){
            //AHORA DESPLIEGA LA IMAGEN
            System.out.println("vamos a desplegar la imagen");
            //carga la imagen creada por el generador de graficos
            //File file = new File("C:\\Users\\Audel\\Desktop\\chart.jpg");
            //File file = new File(pathline);
            File file = new File("C:\\Users\\Audel\\Desktop\\"+pathline);
            BufferedImage imagenEntrante = null;
            if (file != null) {
                try {
                    imagenEntrante = ImageIO.read(file);

                } catch (FileNotFoundException fnfe) {
                    System.out.println("Error abriendo el archivo: " +fnfe);
                } catch (IOException ioe) {
                    System.out.println("Error durante la lectura del archivo: " +ioe);
                }
            } else {
                System.out.println("El archivo es null");
                jLabel1.setText("No se pudo generar la grafica");
            }

            jLabel1.setIcon((new ImageIcon(imagenEntrante)));
            //this.setSize(imagenEntrante.getHeight(), imagenEntrante.getWidth());
            this.setSize(1050, 600);
            repaint();
            }
    }

    private VistaGraficaPorMesTEG() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 300, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /*
         * Set the Nimbus look and feel
         */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /*
         * If Nimbus (introduced in Java SE 6) is not available, stay with the
         * default look and feel. For details see
         * http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VistaGraficaPorMesTEG.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VistaGraficaPorMesTEG.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VistaGraficaPorMesTEG.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VistaGraficaPorMesTEG.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /*
         * Create and display the form
         */
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new VistaGraficaPorMesTEG().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    // End of variables declaration//GEN-END:variables
}
